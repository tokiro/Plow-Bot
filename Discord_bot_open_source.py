import discord
import asyncio

if not discord.opus.is_loaded():
    discord.opus.load_opus('./opus.dll')

class Bot(discord.Client):
    player = None


    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')

    async def on_message(self, message):

        if message.content.startswith('!join'):
            if self.is_voice_connected():
                await self.send_message(message.channel, 'Already connected to a voice channel')
            channel_name = message.content[5:].strip()
            check = lambda c: c.name == channel_name and c.type == discord.ChannelType.voice
            channel = discord.utils.find(check, message.server.channels)
            if channel is None:
                await self.send_message(message.channel, 'Cannot find a voice channel by that name.')
            else:
                await self.join_voice_channel(channel)
                self.starter = message.author

        elif message.content.startswith('!leave'):
            self.starter = None
            await self.voice.disconnect()

        elif message.content.startswith('!stop'):
            if (self.player.is_playing()):
                self.player.stop()

        elif message.content.startswith('!chiale'):
            if (self.voice == None):
                await self.send_message(message.channel, 'Not on a chan MOTHER FUCKER (!join [Channel])')
                return
            if (self.player.is_playing()):
                self.player.stop()
            self.player = await self.voice.create_ytdl_player('https://www.youtube.com/watch?v=vFoEolYzLKc')
            self.player.start()

        elif message.content.startswith('!help'):
            await self.send_message(message.channel, '; !join ; !help ; !leave ; !stop ; !chiale ;')

bot = Bot()
bot.run('[TOKEN A MODIFIER]')
